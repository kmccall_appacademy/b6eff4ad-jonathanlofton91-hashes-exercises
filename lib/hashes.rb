# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split(" ")
  hash = Hash.new(0)
  words.each { |word| hash[word] = word.length }
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  large_num = 0
  large_key = 0
  hash.each do |key, value|
    if value > large_num
      large_num = value
      large_key = key
    end
  end
  large_key
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older.include?(key) ? older[key] = newer[key] : older[key] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.chars.each { |ch| hash[ch] += 1 }
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each { |num| hash[num] += 1 }
  new_array = []
  new_array = hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = { even: 0, odd: 0 }
  numbers.each do |num|
    num.odd? ? hash[:odd] += 1 : hash[:even] += 1
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = %w[a e i o u y]
  hash = Hash.new(0)
  string.chars.each do |ch|
    hash[ch] += 1 if vowels.include?(ch)
  end
  largest_num = 0
  largest_vowel = "z"
  hash.each do |key, value|
    if value >= largest_num
      largest_num = value
      largest_vowel = key unless key > largest_vowel
    end
  end
  largest_vowel
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half = []
  students.each do |student|
    second_half << student[0] if student[1] > 6
  end
  groupings = []
  second_half.each do |student|
    second_half.each do |student2|
      next if student == student2
      groupings << [student, student2] unless groupings.include?([student2,student])
    end
  end
  groupings
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hash = Hash.new(0)
  specimens.each { |animal| hash[animal] += 1 }
  num_species = hash.keys.length
  largest_population = hash.sort_by { |key,value| value }.last.last
  smallest_population = hash.sort_by { |key,value| value }.first.last

  num_species ** 2 * smallest_population / largest_population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_count = character_count(normal_sign)
  vandalized_sign_count = character_count(vandalized_sign)
  vandalized_sign_count.each do |key, value|
    return false if !normal_sign_count.include?(key)
    if normal_sign_count[key] >= vandalized_sign_count[key]
      next
    else
      return false
    end
  end
  true
end

def character_count(str)
  hash = Hash.new(0)
  str.chars.each {|ch| hash[ch.downcase] += 1}
  hash
end
